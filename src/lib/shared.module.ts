import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgSpinKitModule } from 'ng-spin-kit';

import { LocalizedDatePipe } from './localized-date.pipe';
import { LangswitchComponent } from './langswitch/langswitch.component';
import { MsgboxComponent } from './msgbox/msgbox.component';
import { DialogComponent } from './modals/dialog.component';
import { SpinnerComponent } from './modals/spinner.component';
import { LangComponent } from './lang-component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    NgSpinKitModule,
],
declarations: [
    LocalizedDatePipe,
    MsgboxComponent,
    LangComponent,
    LangswitchComponent,
    DialogComponent,
    SpinnerComponent],
entryComponents: [
    DialogComponent,
    SpinnerComponent
],
exports: [
    LocalizedDatePipe,
    MsgboxComponent,
    LangComponent,
    LangswitchComponent,
    DialogComponent,
    SpinnerComponent]
})
export class SharedModule { }
