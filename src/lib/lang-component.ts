import { Component, OnInit } from '@angular/core';
import { filter } from 'rxjs/operators';
import {ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import {ConfigurationService} from './services/configuration.service';
import {TranslateService} from '@ngx-translate/core';

/**
 *
 * The Component displayed on dashboard that changes between Languages
 * @export
 * @class LangComponent
 * @implements {OnInit}
 */
@Component({
    selector: 'app-lang-msgbox',
    template: '<div></div>'
})
export class LangComponent implements OnInit {

    public previousUrl: string;

    constructor(private _config: ConfigurationService,
                private _translate: TranslateService,
                private _router: Router,
                private activatedRoute: ActivatedRoute) {
        this._router.events.pipe(
            filter(event => event instanceof NavigationEnd))
            .subscribe( (ev: any) => {
                this.previousUrl = ev.url;
            });
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            localStorage.setItem('currentLang', params.id);
            this._translate.use(params.id);
            this._router.navigate(['/']);
        });
    }



}
