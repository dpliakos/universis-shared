import { Component} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

/**
 *
 * LanguageSwitch Component
 * @export
 * @class LangswitchComponent
 */
@Component({
  selector: 'app-langswitch',
  templateUrl: './langswitch.component.html'
})

export class LangswitchComponent {

  public currentLang: string;
  public languages: string[];
  private config: any;
  constructor(private translate: TranslateService) { this.initLanguages(); }

  /**
   *
   * Initializes used Languages in the project
   * @memberof LangswitchComponent
   */
  initLanguages() {
    this.currentLang = this.translate.currentLang;
    this.languages = this.config.settings.localization.cultures;
    this.languages = this.languages.filter(item => item !== this.currentLang);
  }

  /**
   *
   * Changes selected Language
   * @param {*} lang
   * @memberof LangswitchComponent
   */
  changeLang(lang) {
    this.config.setCurrentLang(lang);
    this.initLanguages();
  }
}
