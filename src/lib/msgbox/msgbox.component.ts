import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 *
 * Component for Card-Box element with Inputs:
 * Usage <app-msgbox [title]="HelloWorld" ...></app-msgbox>
 * @Input() title: Title of Box
 * @Input() icon: Icon displayed on the left of the element
 * @Input() info: The Status displayed on user
 * @Input() message: Explanation of the status
 * @Input() extraMessage: Some extra guidence
 * @Input() actionButton: Text displayed as Text in button
 * @Input() actionText: Text displayed as an action
 * @Input() disableBut: Disable button
 * @export
 * @class MsgboxComponent
 */
@Component({
  selector: 'app-msgbox',
  templateUrl: './msgbox.component.html',
  styleUrls: ['./msgbox.component.scss']
})
export class MsgboxComponent {

  @Input() title: string;
  @Input() icon: string;
  @Input() info: string;
  @Input() message: string;
  @Input() extraMessage: string;
  @Input() actionButton: string;
  @Input() actionText: string;
  @Input() disableBut: boolean;
  // Usage (action)="someFunction()"
  @Output() action = new EventEmitter<any>();

  btnClicked: boolean;

  clicked() {
    if (!this.btnClicked) {
      this.action.emit();
      this.btnClicked = true;
    }
  }
}
