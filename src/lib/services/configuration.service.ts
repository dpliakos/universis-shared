import { Injectable, Injector } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { registerLocaleData } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { HttpResponse } from "@angular/common/http/src/response";
import { isDevMode } from '@angular/core';
import { DATA_CONTEXT_CONFIG } from "@themost/angular/client";

export declare interface ApplicationSettingsConfiguration {
    name?: string,
    image?: string,
    description?: string,
    thumbnail?: string
}

export declare interface RemoteSettingsConfiguration {
  server?: string
}

export declare interface LocalizationSettingsConfiguration {
  cultures?: Array<string>,
  default?: string,
    append?: any
}

export declare interface SettingsConfiguration {
  app?: ApplicationSettingsConfiguration;
  remote?: RemoteSettingsConfiguration;
  localization?: LocalizationSettingsConfiguration;
  auth?: any;
}

declare var System;

/**
 *
 * This Service is used to retrive/set global Configs for the project
 * @export
 * @class ConfigurationService
 */
@Injectable()
export class ConfigurationService {

  public config;
  constructor(private _translate: TranslateService,
    private _injector: Injector,
    private _http: HttpClient) {
    //
  }

  /**
   *
   * Load Configs saved in Project
   * @returns {Promise<any>}
   * @memberof ConfigurationService
   */
  public load(): Promise<any> {
    if (this.config) {
      return Promise.resolve(true);
    }
    //get environment
    let env = isDevMode() ? 'development' : 'production';
    
    //load configuration
    return this.loadFrom(`assets/config/app.${env}.json`).catch((err) => {
      if (err.status === 404) { //not found
        //load default application configuration
        return this.loadFrom(`assets/config/app.json`)
      }
      return Promise.reject(err);
    });
  }

  private loadFrom(url): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this._http.get(url).subscribe((responseData) => {
        //set configuration data
        this.config = responseData;
        //get data context config by specifying DATA_CONTEXT_CONFIG injection token
        let dataContextConfig = this._injector.get(DATA_CONTEXT_CONFIG);
        //IMPORTANT: SET DATA_CONTEXT_CONFIG base URI
        dataContextConfig.base = this.config.settings.remote.server;
        //load cultures
          let sources = this.config.settings.localization.cultures.map((x)=> {
              let culture = x.substr(0, 2);
              return import(`@angular/common/locales/${culture}.js`).then((module)=> {
                  registerLocaleData(module.default);
                  //get translations
                  return import(`src/assets/i18n/${culture}.json`).then((translations) => {
                      this._translate.setTranslation(culture, translations, true);
                      //append configuration translations
                      if (this.settings.localization.append) {
                          if (this.settings.localization.append.hasOwnProperty(culture)) {
                              this._translate.setTranslation(culture, this.settings.localization.append[culture], true);
                          }
                      }
                      return Promise.resolve();
                  });
              });
          });
          Promise.all(sources).then(()=> {
              resolve(true);
          }).catch((err)=> {
              console.log(err);
              reject(err);
          });
      }, (err) => {
        return reject(err);
      });
    });
  }

  /**
   *
   * Get ConfigSettings
   * @readonly
   * @type {SettingsConfiguration}
   * @memberof ConfigurationService
   */
  get settings(): SettingsConfiguration {
    return this.config.settings;
  }

  /**
   *
   * Get Current used Language
   * @returns {string}
   * @memberof ConfigurationService
   */
  getCurrentLang(): string {
    let currentLang = localStorage.getItem('currentLang');
    if (currentLang) {
      return currentLang;
    }
    this.setCurrentLang(this.settings.localization.default);
    return this.settings.localization.default;
  }

  setCurrentLang(lang: string) {
    localStorage.setItem('currentLang', lang);
    this._translate.use(lang);
    // Working fine without Reloading page
    // window.location.reload();
  }

}
