import { Injectable, EventEmitter} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DialogComponent } from '../modals/dialog.component';
import { ToastrService } from 'ngx-toastr';

/**
 *
 * Displays a Modal window or a type of Notification (based on choice the color changes)
 * @export
 * @class ModalService
 */
@Injectable()
export class ModalService {
  private modalRef: BsModalRef;



  public choice: string;
  config = {
    ignoreBackdropClick: true,
    keyboard: false,
    initialState: null,
    class: 'modal-content-base'
  };

  constructor(private modalService: BsModalService, private toastr: ToastrService) { }

  // Create a modal with custom html passed
  openModal(template: any): BsModalRef {
    return this.modalRef = this.modalService.show(template, this.config);
  }

  /**
   *
   *
   * @param {string} title Set Modal Title
   * @param {string} description Set Modal Description
   * @param {boolean} [isAlert] If true Modal is and Alert box with one Confirm Button
   * @returns {Promise<boolean>} Confirmed = True
   * @memberof ModalService
   */
  openDialog(title: string, description: string, isAlert?: boolean): Promise<boolean> {
    let configInitialized = JSON.parse(JSON.stringify(this.config));
    configInitialized.initialState = { 'title': title, 'description': description, 'isAlert': isAlert};
    const answer = this.modalService.show(DialogComponent, configInitialized).content.choice as EventEmitter<boolean>;
    return new Promise<boolean> ((resolv) => {
      answer.subscribe(res => {
        resolv(res);
      });
    });
  }

  /**
   *
   * Display Success Notification
   * @param {string} title Title of Notification
   * @param {string} description Description of Notification
   * @memberof ModalService
   */
  notifySuccess(title: string, description: string) {
    this.toastr.success(description, title);
  }

  /**
   *
   * Display Error Notification
   * @param {string} title Title of Notification
   * @param {string} description Description of Notification
   * @memberof ModalService
   */
  notifyError(title: string, description: string) {
    this.toastr.error(description, title);
  }

  /**
   *
   * Display Info Notification
   * @param {string} title Title of Notification
   * @param {string} description Description of Notification
   * @memberof ModalService
   */
  notifyInfo(title: string, description: string) {
    this.toastr.info(description, title);
  }

  /**
   *
   * Display Warning Notification
   * @param {string} title Title of Notification
   * @param {string} description Description of Notification
   * @memberof ModalService
   */
  notifyWarning(title: string, description: string) {
    this.toastr.warning(description, title);
  }
}
