/*
 * Public API Surface of shared
 */

export { ConfigurationService } from './lib/services/configuration.service';
export { LangComponent } from './lib/lang-component';
export { ModalService } from './lib/services/modal.service';
export { LoadingService } from './lib/services/loading.service';
export { LocalizedDatePipe } from './lib/localized-date.pipe';
export * from './lib/shared.module';
